import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/core/values/colors.dart';
import 'app/data/providers/db_provider.dart';
import 'app/data/services/settings_service.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  await MoneyLogsDatabase.instance.database;
  runApp(
    GetMaterialApp(
      title: "Money Logs",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: customGraySwatch),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}

Future<void> initServices() async {
  debugPrint('Starting Services');
  await Get.putAsync(() => SettingsService().init());
  debugPrint('Services started');
}
