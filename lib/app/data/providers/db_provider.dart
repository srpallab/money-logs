import 'dart:developer';
import 'dart:io';

import 'package:csv/csv.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../core/models/account_head_fields.dart';
import '../../core/models/account_head_model.dart';
import '../../core/models/income_expanse_model.dart';
import '../../core/models/income_expense_fields.dart';
import '../../modules/register/user_model.dart';
import '../enums/enums.dart';
import '../services/settings_service.dart';

class MoneyLogsDatabase {
  static final MoneyLogsDatabase instance = MoneyLogsDatabase._init();
  static Database? _database;
  final SettingsService _settingsService = Get.put(SettingsService());

  final String userTableName = 'user';

  MoneyLogsDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB('money_logs.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const intType = 'INTEGER NOT NULL';

    log('Creating User Table');
    db.execute('''
      CREATE TABLE  $userTableName (
        id $idType,
        full_name $textType,
        username $textType,
        password $textType 
      )
    ''');

    log('Creating Account Head Table');
    db.execute('''
      CREATE TABLE ${AccountHeadFields.accountHeadTableName} (
        ${AccountHeadFields.id} $idType,
        ${AccountHeadFields.name} $textType,
        ${AccountHeadFields.type} $intType,
        ${IncomeExpenseFields.createdBy} $intType,
        ${IncomeExpenseFields.createdAt} $textType
      )
    ''');

    log('Creating Income Expanse Table');
    db.execute('''
      CREATE TABLE ${IncomeExpenseFields.incomeAndExpenseTableName} (
        ${IncomeExpenseFields.id} $idType,
        ${IncomeExpenseFields.amount} $textType,
        ${IncomeExpenseFields.details} $textType,
        ${IncomeExpenseFields.transactionDate} $textType,
        ${IncomeExpenseFields.transactionTime} $textType,
        ${IncomeExpenseFields.type} $intType,
        ${IncomeExpenseFields.headName} $textType,
        ${IncomeExpenseFields.createdBy} $intType,
        ${IncomeExpenseFields.createdAt} $textType
      )
    ''');
  }

  Future<User> register(User user) async {
    final db = await instance.database;
    int insertId = await db.insert(userTableName, user.toJson());
    return user.copy(id: insertId);
  }

  Future<User> login(String username, String password) async {
    final db = await instance.database;
    final userMap = await db.query(
      userTableName,
      where: 'username = ? AND password = ? ',
      whereArgs: [username, password],
    );
    // log(userMap.toString());
    if (userMap.isNotEmpty) {
      return User.fromJson(userMap.first);
    } else {
      throw Exception('User not Found');
    }
  }

  Future<bool> isIncomeExpenseAccountHeadsPresent() async {
    final db = await instance.database;
    final rowCount = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT(*) FROM ${AccountHeadFields.accountHeadTableName}"));
    // log(rowCount.toString());
    return rowCount! > 0 ? false : true;
  }

  Future<List<AccountHead>> incomeExpenseAccountHeadsByType(
    AccountHeadType accountHeadType,
  ) async {
    final db = await instance.database;
    final incomeAccountHeadsMap = await db.query(
      AccountHeadFields.accountHeadTableName,
      where: "type = ?",
      whereArgs: [accountHeadType.index],
    );
    return List.generate(
      incomeAccountHeadsMap.length,
      (index) => AccountHead(
        id: incomeAccountHeadsMap[index][AccountHeadFields.id] as int,
        name: incomeAccountHeadsMap[index][AccountHeadFields.name] as String,
        type: incomeAccountHeadsMap[index][AccountHeadFields.type] as int,
        createAt:
            incomeAccountHeadsMap[index][AccountHeadFields.createdAt] as String,
        createdBy:
            incomeAccountHeadsMap[index][AccountHeadFields.createdBy] as int,
      ),
    );
  }

  Future<void> addAccountHead(AccountHead accountHead) async {
    final db = await instance.database;
    await db.insert(
        AccountHeadFields.accountHeadTableName, accountHead.toJson());
  }

  Future<bool> showDeleteButton(AccountHead accountHead) async {
    final db = await instance.database;
    final rowCount = Sqflite.firstIntValue(
      await db.rawQuery("SELECT COUNT(*) FROM " +
          IncomeExpenseFields.incomeAndExpenseTableName +
          " WHERE ${IncomeExpenseFields.headName} = '${accountHead.name}'"),
    );
    // log(rowCount.toString());
    return rowCount! > 0 ? false : true;
  }

  Future<void> deleteAccountHead(AccountHead accountHead) async {
    final db = await instance.database;
    await db.delete(
      AccountHeadFields.accountHeadTableName,
      where: "id = ?",
      whereArgs: [accountHead.id],
    );
  }

  Future<void> addIncome(IncomeExpanse income) async {
    final db = await instance.database;
    await db.insert(
        IncomeExpenseFields.incomeAndExpenseTableName, income.toJson());
  }

  Future<void> addExpense(IncomeExpanse expense) async {
    final db = await instance.database;
    await db.insert(
        IncomeExpenseFields.incomeAndExpenseTableName, expense.toJson());
  }

  Future<List<IncomeExpanse>> allIncomeExpanseStatements() async {
    final db = await instance.database;
    final incomeMap =
        await db.query(IncomeExpenseFields.incomeAndExpenseTableName);
    // log(incomeMap.toString());
    return List<IncomeExpanse>.generate(
      incomeMap.length,
      (index) => IncomeExpanse(
        id: incomeMap[index][IncomeExpenseFields.id] as int,
        amount: incomeMap[index][IncomeExpenseFields.amount] as String,
        details: incomeMap[index][IncomeExpenseFields.details] as String,
        type: incomeMap[index][IncomeExpenseFields.type] as int,
        headName: incomeMap[index][IncomeExpenseFields.headName] as String,
        createdBy: incomeMap[index][IncomeExpenseFields.createdBy] as int,
        createdAt: incomeMap[index][IncomeExpenseFields.createdAt] as String,
        transactionDate:
            incomeMap[index][IncomeExpenseFields.transactionDate] as String,
        transactionTime:
            incomeMap[index][IncomeExpenseFields.transactionTime] as String,
      ),
    );
  }

  Future dbToCsvExport() async {
    final db = await instance.database;
    final incomeMap =
        await db.query(IncomeExpenseFields.incomeAndExpenseTableName);
    final List<List<dynamic>> listOfIncomeAndExpense = [
      [
        "id",
        "amount",
        "details",
        "transaction_date",
        "transaction_time",
        "type",
        "head_name",
        "created_by",
        "created_at"
      ]
    ];
    // log("Map: " + incomeMap.toString());
    for (var data in incomeMap) {
      // log("Data: " + data.toString());
      listOfIncomeAndExpense.add([
        data["id"],
        data["amount"],
        data["details"],
        data["transaction_date"],
        data["transaction_time"],
        data["type"],
        data["head_name"],
        data["created_by"],
        data["created_at"]
      ]);
    }
    log(listOfIncomeAndExpense.toString());
    var csvIncomeExpense =
        const ListToCsvConverter().convert(listOfIncomeAndExpense);
    log("CSV: " + csvIncomeExpense);
    final String directory = "/storage/emulated/0/Download";
    final path =
        "$directory/csv-${DateFormat("dd-MM-yyyy").format(DateTime.now())}.csv";
    log("Path: " + path);
    final File file = File(path);
    await file.writeAsString(csvIncomeExpense);
  }

  Future<void> deleteIncomeExpenseStatement(id) async {
    final db = await instance.database;
    db.delete(IncomeExpenseFields.incomeAndExpenseTableName,
        where: "id = ?", whereArgs: [id]);
  }

  Future<double> totalIncome() async {
    double totalIncome = 0;
    final db = await instance.database;
    final allMap = await db.query(IncomeExpenseFields.incomeAndExpenseTableName,
        columns: [IncomeExpenseFields.amount],
        where: 'type = ?',
        whereArgs: [1]);
    for (var element in allMap) {
      totalIncome += double.parse(element["amount"].toString());
    }
    return totalIncome * _settingsService.globalRate.value;
  }

  Future<double> filteredTotalIncome(String startDate, String endDate) async {
    double totalIncome = 0;
    final db = await instance.database;
    final allMap = await db.rawQuery(
        "SELECT ${IncomeExpenseFields.amount} FROM " +
            IncomeExpenseFields.incomeAndExpenseTableName +
            " WHERE  ${IncomeExpenseFields.transactionDate}" +
            " BETWEEN '$startDate' AND '$endDate' AND type = 1;");
    // " '2022-01-31' ;");
    for (var element in allMap) {
      totalIncome += double.parse(element["amount"].toString());
    }
    return totalIncome * _settingsService.globalRate.value;
  }

  Future<double> filteredTotalExpense(String startDate, String endDate) async {
    double totalExpanse = 0;
    final db = await instance.database;
    final allMap = await db.rawQuery(
        "SELECT ${IncomeExpenseFields.amount} FROM " +
            IncomeExpenseFields.incomeAndExpenseTableName +
            " WHERE  ${IncomeExpenseFields.transactionDate}" +
            " BETWEEN '$startDate' AND '$endDate' AND type = 2;");
    for (var element in allMap) {
      totalExpanse += double.parse(element["amount"].toString());
    }
    return totalExpanse * _settingsService.globalRate.value;
  }

  Future<double> totalExpense() async {
    double totalExpanse = 0;

    final db = await instance.database;
    final allMap = await db.query(IncomeExpenseFields.incomeAndExpenseTableName,
        columns: [IncomeExpenseFields.amount],
        where: 'type = ?',
        whereArgs: [2]);
    for (var element in allMap) {
      totalExpanse += double.parse(element["amount"].toString());
    }
    return totalExpanse * _settingsService.globalRate.value;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
