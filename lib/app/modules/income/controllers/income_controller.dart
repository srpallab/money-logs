import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/models/income_expanse_model.dart';
import '../../../data/enums/enums.dart';
import '../../../data/providers/db_provider.dart';

class IncomeController extends GetxController {
  final TextEditingController incomeAmountField = TextEditingController();
  final TextEditingController incomeDetailField = TextEditingController();
  final GlobalKey<FormState> incomeFormKey = GlobalKey<FormState>();
  final incomeHeadName = "".obs;

  addIncome(IncomeExpanse income) async {
    await MoneyLogsDatabase.instance.addIncome(income);
  }

  Future<List<AccountHead>> getAllIncomeHeads() async {
    return await MoneyLogsDatabase.instance
        .incomeExpenseAccountHeadsByType(AccountHeadType.income);
  }

  String? amountValidation(String amount) {
    if (amount.isEmpty) {
      return "Amount Can't be empty";
    } else if (int.parse(amount) <= 2 || int.parse(amount) >= 200000) {
      return "Amount should be more than 2 and less then 200000";
    }
    return null;
  }

  String? detailValidation(String detail) {
    if (detail.isEmpty) {
      return "Detail Can't be empty";
    }
    return null;
  }

  void validateExpenseForm(selectedDate, selectedTime) async {
    final bool isValid = incomeFormKey.currentState!.validate();
    if (!isValid) {
      return;
    } else if (incomeHeadName.value.isEmpty) {
      Get.snackbar("Error", "Please Select a Head Type");
      return;
    } else {
      log("Income Head: " + incomeHeadName.value);
      log("Income Amount: " + incomeAmountField.text);
      log("Income Detail: " + incomeDetailField.text);
      log("Income Date: " + selectedDate);
      log("Income Time: " + selectedTime);

      await MoneyLogsDatabase.instance.addIncome(
        IncomeExpanse(
          amount: incomeAmountField.text,
          details: incomeDetailField.text,
          transactionDate: selectedDate,
          transactionTime: selectedTime,
          headName: incomeHeadName.value,
          type: AccountHeadType.income.index,
          createdBy: 1,
          createdAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
        ),
      );

      Get.offNamedUntil("/dashboard", (route) => false);
    }
  }
}
