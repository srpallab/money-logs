import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/shared/controller/date_picker_controller.dart';
import '../../../core/shared/controller/time_picker_controller.dart';
import '../../../core/theme/button_theme.dart';
import '../../../core/theme/input_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../controllers/income_controller.dart';

class IncomeView extends GetView<IncomeController> {
  final IncomeController incomeController = Get.find<IncomeController>();
  final DateController dateController = Get.put(DateController());
  final TimeController timeController = Get.put(TimeController());

  @override
  Widget build(BuildContext context) {
    return IncomeExpanseLayout(
      appbar: AppBar(title: Text('Add Income'), backgroundColor: buttonGreen),
      button: Center(
        child: SubmitButton(
          onTap: () {
            final selectedDate = DateFormat('yyyy-MM-dd')
                .format(dateController.selectedDate.value);
            final selectedTime =
                timeController.selectedTime.value.format(context);
            incomeController.validateExpenseForm(selectedDate, selectedTime);
          },
          title: "ADD",
          buttonColor: buttonGreen,
        ),
      ),
      inputSection: Form(
        key: incomeController.incomeFormKey,
        child: Column(
          children: [
            AmountInputField(
              controller: incomeController.incomeAmountField,
              onSaved: (value) {
                incomeController.amountValidation(value);
              },
              validator: (value) {
                return incomeController.amountValidation(value);
              },
            ),
            DetailInputField(
              controller: incomeController.incomeDetailField,
              onSaved: (value) {
                incomeController.detailValidation(value);
              },
              validator: (value) {
                return incomeController.detailValidation(value);
              },
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
      dataSection: FutureBuilder(
        future: incomeController.getAllIncomeHeads(),
        builder:
            (BuildContext context, AsyncSnapshot<List<AccountHead>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return errorSection(snapshot);
            } else if (snapshot.data!.isEmpty) {
              return noDataFoundSection(context);
            } else if (snapshot.hasData) {
              log(snapshot.data!.toString());
              return dataShowSection(context, snapshot, controller);
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Expanded noDataFoundSection(context) {
    return Expanded(
      child: Center(child: const Text("No Income Heads Found")),
    );
  }

  Center errorSection(snapshot) {
    return Center(
      child: Text('${snapshot.error}', style: const TextStyle(fontSize: 18)),
    );
  }

  Widget dataShowSection(context, AsyncSnapshot<List<AccountHead>> snapshot,
      IncomeController controller) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.38,
      child: ListView(
        children: snapshot.data!
            .map(
              (head) => Obx(
                () => RadioListTile<String?>(
                  title: Text(head.name),
                  value: head.name,
                  groupValue: controller.incomeHeadName.value,
                  onChanged: (value) {
                    log(value.toString());
                    controller.incomeHeadName.value = value!;
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
