import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../controllers/chart_controller.dart';

class ChartView extends GetView<ChartController> {
  final ChartController _chartController = Get.find<ChartController>();

  final monthsList = [
    "Life Time",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(
        title: Text('Chart'),
        centerTitle: true,
      ),
      body: Obx(() {
        return Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.5,
                decoration: BoxDecoration(border: Border.all(color: black)),
                child: PieChart(
                  PieChartData(
                    sections: [
                      PieChartSectionData(
                        title: "Income",
                        titleStyle: TextStyles.normal16White,
                        color: buttonGreen,
                        value: _chartController.incomeValue.value,
                        radius: 160,
                      ),
                      PieChartSectionData(
                        title: "Expense",
                        titleStyle: TextStyles.normal16White,
                        color: buttonRed,
                        value: _chartController.expenseValue.value,
                        radius: 160,
                      ),
                    ],
                    centerSpaceRadius: 0,
                    sectionsSpace: 2,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            DropdownButton<String>(
              value: _chartController.selectedMonth.value,
              items: monthsList
                  .map(
                    (e) => DropdownMenuItem<String>(
                      value: e,
                      child: Text(
                        e,
                        style: TextStyles.normal16Black,
                      ),
                    ),
                  )
                  .toList(),
              onChanged: (value) async {
                _chartController.selectedMonth(value);
                await _chartController.getIncomeValueByFilter(value!);
              },
            ),
          ],
        );
      }),
    );
  }
}
