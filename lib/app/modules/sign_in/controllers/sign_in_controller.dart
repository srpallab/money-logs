import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../data/providers/db_provider.dart';
import '../../../data/services/settings_service.dart';

class SignInController extends GetxController {
  final SettingsService settingsService;

  SignInController({required this.settingsService});

  final GlobalKey<FormState> signInFormKey = GlobalKey<FormState>();

  final TextEditingController usernameCtl = TextEditingController();
  final TextEditingController passwordCtl = TextEditingController();

  RxBool isLoading = false.obs;

  @override
  onInit() async {
    var _preferences = await SharedPreferences.getInstance();
    usernameCtl.text = _preferences.getStringList("user")?[1] ?? "";
    super.onInit();
  }

  String? usernameValidation(String username) {
    if (username.isEmpty) {
      return "Username Can't be empty";
    } else if (!GetUtils.isUsername(username)) {
      return "It is not a valid username";
    }
    return null;
  }

  String? passwordValidation(String password) {
    if (password.isEmpty) {
      return "Password Can't be empty";
    }
    return null;
  }

  void signIn() async {
    final bool isValid = signInFormKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    signInFormKey.currentState!.save();
    log("Username: " + usernameCtl.text);
    log("Password: " + passwordCtl.text);

    isLoading.value = true;
    try {
      final user = await MoneyLogsDatabase.instance
          .login(usernameCtl.text, passwordCtl.text);
      settingsService.userAdded([user.fullName ?? "", user.username ?? ""]);
      Get.offNamedUntil('/dashboard', (route) => false);
    } catch (e) {
      Get.snackbar("Message", "No User Found");
    }
    isLoading.value = false;
  }
}
