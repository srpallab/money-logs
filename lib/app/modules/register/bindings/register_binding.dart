import 'package:get/get.dart';

import '../../../data/services/settings_service.dart';
import '../controllers/register_controller.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterController>(
      () => RegisterController(
        settingsService: Get.find<SettingsService>(),
      ),
    );
  }
}
