import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../../../data/services/settings_service.dart';
import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  final SettingsService _settingsService = Get.find<SettingsService>();

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(title: Text('Settings'), centerTitle: true),
      body: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 150,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [settingsFirstColor, settingsSecondColor],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "KaziSpin TecHub",
                  style: TextStyles.bold24White,
                ),
                Text(
                  "we develop, we deploy, we deliver",
                  style: TextStyles.normal16White,
                ),
                cardSection(
                    Icons.account_box_outlined, _settingsService.userList[0]),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: InkWell(
              child: ListTile(
                leading: Icon(Icons.monetization_on_outlined),
                trailing: Icon(Icons.chevron_right),
                title: Text("Change Currency"),
              ),
              onTap: () {
                Get.toNamed("/currency");
              },
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 18.0),
          //   child: Divider(color: black, thickness: 2),
          // ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 8.0),
          //   child: ListTile(
          //     leading: Icon(Icons.monetization_on_outlined),
          //     trailing: Icon(Icons.chevron_right),
          //     title: Text("Set Rate"),
          //   ),
          // ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 18.0),
          //   child: Divider(color: black, thickness: 2),
          // ),
        ],
      ),
    );
  }

  Padding cardSection(IconData icon, String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(icon, size: 20),
            Text(text, style: TextStyles.normal16Black),
            SizedBox(width: 5, height: 40)
          ],
        ),
      ),
    );
  }
}
