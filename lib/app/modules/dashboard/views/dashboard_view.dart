import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:money_logs/app/data/services/settings_service.dart';

import '../../../core/models/income_expanse_model.dart';
import '../../../core/theme/button_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  final DashboardController _dashboardController =
      Get.find<DashboardController>();
  final SettingsService _settingsService = Get.put(SettingsService());

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(title: Text('Dashboard')),
      body: body(context),
    );
  }

  Container body(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: GetBuilder<DashboardController>(
        builder: (logic) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: totalCardSection(),
              ),
              FutureBuilder(
                future: _dashboardController.getAllIncomeExpanse(),
                builder: (BuildContext ctx,
                    AsyncSnapshot<List<IncomeExpanse>> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return errorSection(snapshot);
                    } else if (snapshot.data!.isEmpty) {
                      return noDataFoundSection();
                    } else if (snapshot.hasData) {
                      return dataShowSection(snapshot);
                    }
                  }
                  return const Center(child: CircularProgressIndicator());
                },
              ),
              SizedBox(height: 10),
              gotoIncomePage(),
              SizedBox(height: 10),
              gotoExpensePage(),
              SizedBox(height: 10),
            ],
          );
        },
      ),
    );
  }

  Expanded dataShowSection(AsyncSnapshot<List<IncomeExpanse>> snapshot) {
    log(_settingsService.globalRate.toString());
    return Expanded(
      child: ListView.builder(
        itemCount: snapshot.data!.length,
        itemBuilder: (context, index) {
          return Slidable(
            startActionPane: ActionPane(
              extentRatio: 1 / 5,
              motion: ScrollMotion(),
              children: [
                SlidableAction(
                  onPressed: (context) {
                    _dashboardController
                        .deleteStatement(snapshot.data![index].id);
                  },
                  backgroundColor: buttonRed,
                  foregroundColor: Colors.white,
                  icon: Icons.delete,
                  label: 'Delete',
                ),
              ],
            ),
            child: ListTile(
              isThreeLine: false,
              onLongPress: () {
                Get.defaultDialog(
                  title: "Transaction Details",
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      transactionHeadSection(snapshot, index),
                      SizedBox(height: 10),
                      transactionAmountSection(snapshot, index),
                      SizedBox(height: 10),
                      transactionACTypeSection(snapshot, index),
                      SizedBox(height: 10),
                      Text("Detail: " + snapshot.data![index].details),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Created At: "),
                          Text(snapshot.data![index].createdAt),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Transaction Date: "),
                          Text(DateFormat('dd-MM-yyyy')
                              .format(DateTime.parse(
                                  snapshot.data![index].transactionDate!))
                              .toString()),
                        ],
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                );
              },
              leading: snapshot.data![index].type == 1
                  ? Icon(Icons.add, color: buttonGreen)
                  : Icon(Icons.remove, color: buttonRed),
              title: dashboardTitleSection(snapshot, index),
              subtitle: Text(
                snapshot.data![index].details,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              // isThreeLine: false,

              trailing: Text(
                (double.parse(snapshot.data![index].amount) *
                        _settingsService.globalRate.value)
                    .toStringAsFixed(2),
                style: snapshot.data![index].type == 1
                    ? TextStyles.normal16Green
                    : TextStyles.normal16Red,
              ),
            ),
          );
        },
      ),
    );
  }

  Row dashboardTitleSection(
      AsyncSnapshot<List<IncomeExpanse>> snapshot, int index) {
    return Row(
      children: [
        Expanded(
          child: Text(
            snapshot.data![index].headName!,
            style: snapshot.data![index].type == 1
                ? TextStyles.normal16Green
                : TextStyles.normal16Red,
          ),
        ),
        Text(
          snapshot.data![index].createdAt,
          style: TextStyles.normal12Black,
        ),
        SizedBox(width: 10),
        Text(
          DateFormat('dd-MM-yyyy')
              .format(DateTime.parse(snapshot.data![index].transactionDate!))
              .toString(),
          style: TextStyles.normal12Black,
        ),
      ],
    );
  }

  Row transactionACTypeSection(
      AsyncSnapshot<List<IncomeExpanse>> snapshot, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Account Type: "),
        Text(snapshot.data![index].type == 1 ? "Income" : "Expense"),
      ],
    );
  }

  Row transactionAmountSection(
      AsyncSnapshot<List<IncomeExpanse>> snapshot, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Amount: "),
        Text(snapshot.data![index].amount),
      ],
    );
  }

  Row transactionHeadSection(
      AsyncSnapshot<List<IncomeExpanse>> snapshot, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Account Head: "),
        Text(snapshot.data![index].headName!),
      ],
    );
  }

  Center errorSection(snapshot) {
    return Center(
      child: Text(
        snapshot.error.toString(),
        style: const TextStyle(fontSize: 18),
      ),
    );
  }

  Expanded noDataFoundSection() {
    return Expanded(
      child: Center(
        child: Text(
          'No Income or Expense Statement Found!',
          style: TextStyles.normal16Black,
        ),
      ),
    );
  }

  SubmitButton gotoExpensePage() {
    return SubmitButton(
      buttonColor: buttonRed,
      onTap: () {
        Get.toNamed("/expense");
      },
      title: 'Add Expense',
    );
  }

  SubmitButton gotoIncomePage() {
    return SubmitButton(
      buttonColor: buttonGreen,
      onTap: () {
        Get.toNamed("/income");
      },
      title: 'Add Income',
    );
  }

  Widget totalCardSection() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [incomeSection(), expenseSection(), totalSection()],
        ),
      ),
    );
  }

  Chip totalSection() {
    return Chip(
      label: Obx(
        () => Text(
          (_dashboardController.totalIncomeDouble.value -
                  _dashboardController.totalExpenseDouble.value)
              .toStringAsFixed(2),
          style: TextStyles.normal12Black,
        ),
      ),
      backgroundColor: Colors.black12,
    );
  }

  Widget expenseSection() {
    return Chip(
      label: Obx(
        () => Text(
          _dashboardController.totalExpenseDouble.toStringAsFixed(2),
          style: TextStyles.normal12White,
        ),
      ),
      backgroundColor: buttonRed,
      avatar: const CircleAvatar(
        child: Icon(Icons.remove, color: buttonRed),
        backgroundColor: white,
      ),
    );
  }

  Widget incomeSection() {
    return Chip(
      label: Obx(
        () => Text(
          _dashboardController.totalIncomeDouble.value.toStringAsFixed(2),
          style: TextStyles.normal12White,
        ),
      ),
      backgroundColor: buttonGreen,
      avatar: const CircleAvatar(
        child: Icon(Icons.add, color: buttonGreen),
        backgroundColor: white,
      ),
    );
  }
}
// '৳ ' +
