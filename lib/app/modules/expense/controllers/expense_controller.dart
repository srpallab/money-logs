import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/models/income_expanse_model.dart';
import '../../../data/enums/enums.dart';
import '../../../data/providers/db_provider.dart';

class ExpenseController extends GetxController {
  final TextEditingController expenseAmountField = TextEditingController();
  final TextEditingController expenseDetailField = TextEditingController();
  final GlobalKey<FormState> expenseFormKey = GlobalKey<FormState>();
  final expenseHeadName = "".obs;

  Future<List<AccountHead>> getAllExpenseHeads() async {
    return await MoneyLogsDatabase.instance
        .incomeExpenseAccountHeadsByType(AccountHeadType.expense);
  }

  addExpanse(IncomeExpanse expense) async {
    await MoneyLogsDatabase.instance.addExpense(expense);
  }

  String? amountValidation(String amount) {
    if (amount.isEmpty) {
      return "Amount Can't be empty";
    } else if (int.parse(amount) <= 2 || int.parse(amount) >= 200000) {
      return "Amount should be more than 2 and less then 200000";
    }
    return null;
  }

  String? detailValidation(String detail) {
    if (detail.isEmpty) {
      return "Detail Can't be empty";
    }
    return null;
  }

  void validateExpenseForm(selectedDate, selectedTime) async {
    final bool isValid = expenseFormKey.currentState!.validate();
    if (!isValid && expenseHeadName.value != "") {
      return;
    } else if (expenseHeadName.value.isEmpty) {
      Get.snackbar("Error", "Please Select a Head Type");
      return;
    } else {
      log(expenseAmountField.text);
      log(expenseDetailField.text);
      log(selectedDate);
      log(selectedTime);
      log(DateFormat('dd-MM-yyyy').format(DateTime.now()));

      await MoneyLogsDatabase.instance.addExpense(
        IncomeExpanse(
            amount: expenseAmountField.text,
            details: expenseDetailField.text,
            transactionDate: selectedDate,
            transactionTime: selectedTime,
            headName: expenseHeadName.value,
            type: AccountHeadType.expense.index,
            createdBy: 1,
            createdAt: DateFormat('dd-MM-yyyy').format(DateTime.now())),
      );

      Get.offNamedUntil("/dashboard", (route) => false);
    }
  }
}
