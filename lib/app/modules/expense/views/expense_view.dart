import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/shared/controller/date_picker_controller.dart';
import '../../../core/shared/controller/time_picker_controller.dart';
import '../../../core/theme/button_theme.dart';
import '../../../core/theme/input_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../controllers/expense_controller.dart';

class ExpenseView extends GetView<ExpenseController> {
  final ExpenseController _expanseController = Get.find<ExpenseController>();
  final DateController dateController = Get.put(DateController());
  final TimeController timeController = Get.put(TimeController());

  @override
  Widget build(BuildContext context) {
    return IncomeExpanseLayout(
      appbar: AppBar(title: Text('Add Expanse'), backgroundColor: buttonRed),
      button: SubmitButton(
        onTap: () {
          final selectedDate = DateFormat('yyyy-MM-dd')
              .format(dateController.selectedDate.value);
          final selectedTime =
              timeController.selectedTime.value.format(context);
          _expanseController.validateExpenseForm(selectedDate, selectedTime);
        },
        title: "ADD",
        buttonColor: buttonRed,
      ),
      inputSection: Form(
        key: _expanseController.expenseFormKey,
        child: Column(
          children: [
            AmountInputField(
              controller: _expanseController.expenseAmountField,
              onSaved: (value) {
                _expanseController.expenseAmountField.text = value;
              },
              validator: (value) {
                return _expanseController.amountValidation(value);
              },
            ),
            DetailInputField(
              controller: _expanseController.expenseDetailField,
              onSaved: (value) {
                _expanseController.expenseDetailField.text = value;
              },
              validator: (value) {
                return _expanseController.detailValidation(value);
              },
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
      dataSection: FutureBuilder(
        future: _expanseController.getAllExpenseHeads(),
        builder:
            (BuildContext context, AsyncSnapshot<List<AccountHead>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return errorSection(snapshot);
            } else if (snapshot.data!.isEmpty) {
              return noDataFoundSection();
            } else if (snapshot.hasData) {
              log(snapshot.data!.toString());
              return dataShowSection(context, snapshot, controller);
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Expanded noDataFoundSection() {
    return Expanded(
      child: Center(
        child: Text(
          'No Income or Expense Statements Found!',
          style: TextStyles.normal16Black,
        ),
      ),
    );
  }

  Center errorSection(snapshot) {
    return Center(
      child: Text('${snapshot.error}', style: const TextStyle(fontSize: 18)),
    );
  }

  Widget dataShowSection(context, AsyncSnapshot<List<AccountHead>> snapshot,
      ExpenseController controller) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.36,
      child: ListView(
        children: snapshot.data!
            .map(
              (head) => Obx(
                () => RadioListTile<String?>(
                  title: Text(head.name),
                  value: head.name,
                  groupValue: controller.expenseHeadName.value,
                  onChanged: (value) {
                    log(value!);
                    controller.expenseHeadName.value = value;
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
