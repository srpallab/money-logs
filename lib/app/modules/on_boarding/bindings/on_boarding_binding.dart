import 'package:get/get.dart';

import '../../../data/services/settings_service.dart';
import '../controllers/on_boarding_controller.dart';

class OnBoardingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SettingsService());
    Get.lazyPut<OnBoardingController>(
      () => OnBoardingController(
        settingsService: Get.find<SettingsService>(),
      ),
    );
  }
}
