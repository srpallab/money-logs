import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/core/theme/button_theme.dart';
import 'package:money_logs/app/core/values/colors.dart';

import '../../../core/theme/layout_theme.dart';
import '../controllers/import_export_controller.dart';

class ImportExportView extends GetView<ImportExportController> {
  final ImportExportController _importExportController =
      Get.find<ImportExportController>();

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      isDrawer: true,
      appbar: AppBar(
        title: Text('Import Export'),
        centerTitle: true,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SubmitButton(
              title: "Export Data",
              onTap: () {
                _importExportController.exportDataToDownloadFolder();
              },
              buttonColor: buttonGreen,
            ),
            SizedBox(height: 20),
            SubmitButton(
              title: "Import Data",
              onTap: () {
                _importExportController.importDataToDatabase();
              },
              buttonColor: buttonRed,
            ),
          ],
        ),
      ),
    );
  }
}
