import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:csv/csv.dart';
import 'package:file_picker/file_picker.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../core/models/income_expanse_model.dart';
import '../../../data/providers/db_provider.dart';

class ImportExportController extends GetxController {
  final RxBool permission = false.obs;

  @override
  onInit() {
    askStoragePermission();
    super.onInit();
  }

  Future askStoragePermission() async {
    final PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      permission.value = true;
    } else {
      Get.snackbar(
          "Permission", "Storage Permission is needed to Export or Import");
    }
  }

  exportDataToDownloadFolder() async {
    if (permission.value == true) {
      await MoneyLogsDatabase.instance.dbToCsvExport().then(
            (_) => Get.snackbar("CSV", "File downloaded into Download Folder"),
          );
    } else {
      Get.snackbar(
          "Permission", "Storage Permission is needed to Export or Import");
    }
  }

  importDataToDatabase() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ["csv"],
    );
    if (result == null) {
      return;
    } else {
      log(result.files[0].path.toString());
      // final String directory = "/storage/emulated/0/Download";
      // final input = File("$directory/csv-17-04-2022.csv").openRead();
      final input = File(result.files[0].path!).openRead();
      final fields = await input
          .transform(utf8.decoder)
          .transform(CsvToListConverter())
          .toList();
      log(fields.toString());
      for (var field in fields) {
        if (field[5] == 1) {
          log("Adding Income");
          await MoneyLogsDatabase.instance.addIncome(
            IncomeExpanse(
              amount: field[1].toString(),
              details: field[2],
              transactionDate: field[3],
              transactionTime: field[4],
              type: field[5],
              headName: field[6],
              createdBy: field[7],
              createdAt: field[8],
            ),
          );
        } else if (field[5] == 2) {
          log("Adding Expense");
          await MoneyLogsDatabase.instance.addExpense(
            IncomeExpanse(
              amount: field[1].toString(),
              details: field[2],
              transactionDate: field[3],
              transactionTime: field[4],
              type: field[5],
              headName: field[6],
              createdBy: field[7],
              createdAt: field[8],
            ),
          );
        }
      }
    }
  }
}
