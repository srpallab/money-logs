import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/data/providers/db_provider.dart';
import 'package:money_logs/app/modules/account_head/controllers/account_head_controller.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';

class AccountHeadSection extends GetView<AccountHeadController> {
  final AsyncSnapshot<List<AccountHead>> accHead;
  final Function canDelete;
  const AccountHeadSection(
      {Key? key, required this.accHead, required this.canDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (ctx, index) => Padding(
        padding: const EdgeInsets.only(right: 70.0, left: 70),
        child: const Divider(color: white),
      ),
      itemCount: accHead.data!.length,
      itemBuilder: (ctx, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: ListTile(
              title: Text(
                accHead.data![index].name,
                style: TextStyles.normal16White,
              ),
              leading: const Icon(Icons.account_balance_wallet, color: white),
              trailing: FutureBuilder(
                future: canDelete(accHead.data![index]),
                builder: (context, snapshot) {
                  if (snapshot.hasData && (snapshot.data == true)) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 58.0),
                      child: InkWell(
                        child: const Icon(Icons.delete, color: white),
                        onTap: () async {
                          await MoneyLogsDatabase.instance
                              .deleteAccountHead(accHead.data![index]);
                          Get.offNamed("/dashboard");
                          Get.snackbar("Delete", "Account Head Deleted");
                        },
                      ),
                    );
                  }
                  return SizedBox();
                },
              )),
        );
      },
    );
  }
}
