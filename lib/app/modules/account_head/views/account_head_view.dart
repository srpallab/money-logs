import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../data/enums/enums.dart';
import '../../../data/providers/db_provider.dart';
import '../controllers/account_head_controller.dart';
import 'expense_head_view.dart';
import 'income_head_view.dart';

class AccountHeadView extends GetView<AccountHeadController> {
  final AccountHeadController _accountHeadController =
      Get.find<AccountHeadController>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: MainLayout(
        isDrawer: true,
        appbar: AppBar(
          title: Text('Account Heads'),
          bottom: TabBar(tabs: [Tab(text: "Income"), Tab(text: "Expense")]),
        ),
        body: GetBuilder<AccountHeadController>(
          builder: (_) {
            return TabBarView(children: [IncomeHeadView(), ExpenseHeadView()]);
          },
        ),
        isFloatingButton: true,
        floatingActionButton: FloatingActionButton(
          backgroundColor: buttonGreen,
          child: const Icon(Icons.add),
          onPressed: () {
            addCategoryDialog();
          },
        ),
      ),
    );
  }

  addCategoryDialog() {
    return Get.defaultDialog(
      title: "New Head",
      textConfirm: "Add",
      confirmTextColor: whiteE5,
      buttonColor: buttonGreen,
      textCancel: "Cancel",
      cancelTextColor: buttonRed,
      content:
          Column(children: [accountHeadName(), accountTypeDropdownButton()]),
      onConfirm: () async {
        if (_accountHeadController.accountHeadCtl.text.isEmpty) {
          Get.snackbar("Error", "Head can't be empty", colorText: buttonRed);
        } else if (_accountHeadController.selectedACHeadType.value == 0) {
          Get.snackbar("Error", "Please select a type", colorText: buttonRed);
        } else {
          await MoneyLogsDatabase.instance.addAccountHead(
            AccountHead(
              name: _accountHeadController.accountHeadCtl.text,
              type: _accountHeadController.selectedACHeadType.value,
              createdBy: 1,
              createAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
            ),
          );
          _accountHeadController.accountHeadCtl.text = "";
          _accountHeadController.selectedACHeadType.value = 0;
          _accountHeadController.update();
          Get.back();
        }
      },
    );
  }

  TextFormField accountHeadName() {
    return TextFormField(
      controller: _accountHeadController.accountHeadCtl,
      decoration: InputDecoration(hintText: "Account Head"),
    );
  }

  DropdownButtonFormField<AccountHeadType> accountTypeDropdownButton() {
    final accountHeadType = [AccountHeadType.income, AccountHeadType.expense];

    return DropdownButtonFormField<AccountHeadType>(
      hint: Text("Please select type"),
      items: accountHeadType
          .map(
            (e) => DropdownMenuItem(child: Text(e.name), value: e),
          )
          .toList(),
      onChanged: (value) {
        _accountHeadController.selectedACHeadType.value = value!.index;
      },
    );
  }
}
