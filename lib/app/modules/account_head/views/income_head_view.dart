import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/data/enums/enums.dart';

import '../../../core/models/account_head_model.dart';
import '../../../core/theme/button_theme.dart';
import '../../../core/theme/layout_theme.dart';
import '../../../core/values/colors.dart';
import '../../../core/values/strings.dart';
import '../../../data/providers/db_provider.dart';
import '../controllers/account_head_controller.dart';
import '../widgets/account_head_section.dart';

class IncomeHeadView extends GetView<AccountHeadController> {
  final AccountHeadController _accountHeadController =
      Get.find<AccountHeadController>();

  @override
  Widget build(BuildContext context) {
    return BlankLayout(
      body: FutureBuilder(
        future: MoneyLogsDatabase.instance
            .incomeExpenseAccountHeadsByType(AccountHeadType.income),
        builder: (
          BuildContext context,
          AsyncSnapshot<List<AccountHead>> snapshot,
        ) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return errorSection();
            } else if (snapshot.data!.isEmpty) {
              return noDataSection();
            } else if (snapshot.hasData) {
              return AccountHeadSection(
                accHead: snapshot,
                canDelete: _accountHeadController.checkCanDelete,
              );
            }
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  ListView noDataSection() {
    return ListView(children: [
      ...buildCheckBox(),
      Padding(
        padding: const EdgeInsets.all(18.0),
        child: SubmitButton(
          title: "Add Income Heads",
          onTap: () async {
            log("Tap");
            await _accountHeadController.addIncomeHeads();
          },
          buttonColor: buttonGreen,
        ),
      ),
    ]);
  }

  List<Obx> buildCheckBox() {
    return _accountHeadController.incomeAccountHeadList
        .map(
          (incomeHead) => Obx(() {
            return ListTile(
              title: Text(incomeHead.name!, style: TextStyles.normal16White),
              leading: Checkbox(
                value: incomeHead.add.value,
                onChanged: (value) {
                  incomeHead.add.value = value!;
                },
              ),
            );
          }),
        )
        .toList();
  }

  Center errorSection() {
    return Center(
      child: Text('Error occurred', style: const TextStyle(fontSize: 18)),
    );
  }
}
