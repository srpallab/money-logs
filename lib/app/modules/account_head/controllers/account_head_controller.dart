import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../core/models/account_head_first_list_model.dart';
import '../../../core/models/account_head_model.dart';
import '../../../data/enums/enums.dart';
import '../../../data/providers/db_provider.dart';

class AccountHeadController extends GetxController {
  final TextEditingController accountHeadCtl = TextEditingController();

  final selectedACHeadType = 0.obs;

  final RxList<AccountHeadFirstList> incomeAccountHeadList = [
    AccountHeadFirstList(name: "Salary", add: true.obs),
    AccountHeadFirstList(name: "Transport Allowance", add: true.obs),
    AccountHeadFirstList(name: "Medical Allowance", add: true.obs),
    AccountHeadFirstList(name: "Pocket Money", add: true.obs),
  ].obs;

  final RxList<AccountHeadFirstList> expenseAccountHeadList = [
    AccountHeadFirstList(name: "Food", add: true.obs),
    AccountHeadFirstList(name: "Transport", add: true.obs),
    AccountHeadFirstList(name: "Medical", add: true.obs),
  ].obs;

  addNow(head, type) async {
    await MoneyLogsDatabase.instance.addAccountHead(
      AccountHead(
        name: head.name!,
        type: type.index,
        createdBy: 1,
        createAt: DateFormat('dd-MM-yyyy').format(DateTime.now()),
      ),
    );
  }

  addIncomeHeads() {
    for (var head in incomeAccountHeadList) {
      log(head.name!);
      if (head.add.isTrue) {
        addNow(head, AccountHeadType.income);
      }
    }
    Get.offNamedUntil("/dashboard", (route) => false);
    Get.snackbar("New Heads", "New Head Added");
  }

  addExpanseHeads() {
    for (var head in expenseAccountHeadList) {
      log(head.name!);
      if (head.add.isTrue) {
        addNow(head, AccountHeadType.expense);
      }
    }
    Get.offNamedUntil("/dashboard", (route) => false);
    Get.snackbar("New Heads", "New Head Added");
  }

  Future<bool> checkCanDelete(AccountHead data) async {
    var result = await MoneyLogsDatabase.instance.showDeleteButton(data);
    log(result.toString());
    return result;
  }
}
