import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

class TextStyles {
  // Bold
  static TextStyle bold24 =
      GoogleFonts.poppins(fontSize: 24, fontWeight: FontWeight.w800);

  static final bold24White = bold24.copyWith(color: white);

  // Semi Bold
  static TextStyle semiBold48 =
      GoogleFonts.poppins(fontSize: 48, fontWeight: FontWeight.w600);
  static final semiBold48WhiteE5 = semiBold48.copyWith(color: whiteE5);

  // Medium
  static TextStyle medium25 =
      GoogleFonts.poppins(fontSize: 25, fontWeight: FontWeight.w500);

  static final medium25Black = medium25.copyWith(color: black);

  // Normal
  static TextStyle normal16 =
      GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w400);

  static final normal16Black = normal16.copyWith(color: black);
  static final normal16White = normal16.copyWith(color: white);
  static final normal16Green = normal16.copyWith(color: buttonGreen);
  static final normal16Red = normal16.copyWith(color: buttonRed);

  // Normal
  static TextStyle normal12 =
      GoogleFonts.poppins(fontSize: 12, fontWeight: FontWeight.w400);

  static final normal12White = normal12.copyWith(color: white);
  static final normal12Black = normal12.copyWith(color: black);
  static final normal12Green = normal12.copyWith(color: buttonGreen);
  static final normal12Red = normal12.copyWith(color: buttonRed);
}
