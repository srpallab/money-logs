import 'package:flutter/material.dart';

import '../values/colors.dart';

class CustomInputField extends StatelessWidget {
  final String hint;
  final bool isHidden;
  final Function onSaved;
  final TextEditingController controller;
  final Function validator;

  const CustomInputField({
    Key? key,
    required this.hint,
    required this.onSaved,
    this.isHidden = false,
    required this.controller,
    required this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.black45),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(color: Colors.black26, spreadRadius: 1, blurRadius: 2),
          ],
        ),
        child: buildTextFormField(),
      ),
    );
  }

  TextFormField buildTextFormField() {
    return TextFormField(
      obscureText: isHidden,
      // initialValue: "srp",
      decoration: buildInputDecoration(),
      controller: controller,
      validator: (value) {
        return validator(value);
      },
      onSaved: (value) {
        onSaved(value);
      },
    );
  }

  InputDecoration buildInputDecoration() {
    return InputDecoration(
      label: Text(hint),
      alignLabelWithHint: true,
      hintStyle: const TextStyle(fontSize: 14),
      focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: transparent, width: 0),
      ),
      enabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: transparent, width: 0),
      ),
      errorBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: buttonRed, width: 1),
      ),
    );
  }
}

class AmountInputField extends StatelessWidget {
  final Function onSaved;
  final TextEditingController controller;
  final Function validator;
  const AmountInputField(
      {Key? key,
      required this.onSaved,
      required this.controller,
      required this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Amount',
        labelStyle: TextStyle(color: buttonGreen),
        hintText: 'Min. 2, Max. 200000',
      ),
      controller: controller,
      validator: (value) {
        return validator(value);
      },
      onSaved: (value) {
        onSaved(value);
      },
      keyboardType: TextInputType.number,
    );
  }
}

class DetailInputField extends StatelessWidget {
  final Function onSaved;
  final TextEditingController controller;
  final Function validator;
  const DetailInputField(
      {Key? key,
      required this.onSaved,
      required this.controller,
      required this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Detail',
        labelStyle: TextStyle(color: buttonGreen),
      ),
      maxLines: 3,
      controller: controller,
      validator: (value) {
        return validator(value);
      },
      onSaved: (value) {
        onSaved(value);
      },
    );
  }
}
