import 'package:flutter/material.dart';

import '../values/colors.dart';
import '../widgets/date_picker.dart';
import '../widgets/drawer.dart';
import '../widgets/time_picker.dart';

class BlankLayout extends StatelessWidget {
  final Widget body;
  const BlankLayout({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [firstColor, secondColor],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: body,
      ),
    );
  }
}

class MainLayout extends StatelessWidget {
  final Widget body;
  final AppBar appbar;
  final bool isDrawer;
  final bool isFloatingButton;
  final Widget? floatingActionButton;
  const MainLayout(
      {Key? key,
      required this.body,
      required this.appbar,
      this.isDrawer = false,
      this.isFloatingButton = false,
      this.floatingActionButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteE5,
      appBar: appbar,
      drawer: isDrawer ? TechHubDrawer() : null,
      body: body,
      floatingActionButton: isFloatingButton ? floatingActionButton : null,
    );
  }
}

class IncomeExpanseLayout extends StatelessWidget {
  final Widget button;
  final AppBar appbar;
  final Widget inputSection;
  final Widget dataSection;
  const IncomeExpanseLayout({
    Key? key,
    required this.button,
    required this.appbar,
    required this.inputSection,
    required this.dataSection,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbar,
      backgroundColor: whiteE5,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.only(top: 10, right: 20, left: 20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              inputSection,
              pickersSection(),
              const SizedBox(height: 40),
              const Text("Please Select a Income Head"),
              const SizedBox(height: 20),
              dataSection,
              button
            ],
          ),
        ),
      ),
    );
  }

  Row pickersSection() {
    return Row(
      children: [
        Expanded(child: DatePicker()),
        SizedBox(width: 20),
        Expanded(child: TimePicker()),
      ],
    );
  }
}
