import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../shared/controller/time_picker_controller.dart';
import '../values/colors.dart';

class TimePicker extends StatelessWidget {
  const TimePicker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TimeController timeController = Get.put(TimeController());

    return GestureDetector(
      onTap: () {
        timeController.chooseTime();
      },
      child: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            const Text('Time', style: TextStyle(color: buttonGreen)),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  timeController.selectedTime.value.hour.toString() +
                      " : " +
                      timeController.selectedTime.value.minute.toString(),
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                const Icon(Icons.access_time, color: buttonGreen)
              ],
            ),
            Divider(color: buttonGreen),
          ],
        ),
      ),
    );
  }
}
