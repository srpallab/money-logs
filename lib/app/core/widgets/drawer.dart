import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:money_logs/app/data/services/settings_service.dart';

import '../values/colors.dart';
import '../values/strings.dart';

class TechHubDrawer extends GetView<SettingsService> {
  final SettingsService _settingsService = Get.put(SettingsService());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: primaryColor,
            ),
            child: Text(
              _settingsService.userList[0],
              style: TextStyles.normal16White,
            ),
          ),
          ListTile(
            leading: Icon(Icons.dashboard_outlined),
            title: const Text("Dashboard"),
            onTap: () {
              Get.offNamed('/dashboard');
            },
          ),
          ListTile(
            leading: Icon(Icons.monetization_on_outlined),
            title: const Text("Account Heads"),
            onTap: () {
              Get.offNamed('/account-head');
            },
          ),
          ListTile(
            leading: Icon(Icons.pie_chart_outline_outlined),
            title: const Text("View Chart"),
            onTap: () {
              Get.offNamed('/chart');
            },
          ),
          ListTile(
            leading: Icon(Icons.import_export_outlined),
            title: const Text("Import Export"),
            onTap: () {
              Get.offNamed('/import-export');
            },
          ),
          ListTile(
            leading: Icon(Icons.settings_outlined),
            title: const Text("Settings"),
            onTap: () {
              Get.offNamed('/settings');
            },
          ),
        ],
      ),
    );
  }
}
