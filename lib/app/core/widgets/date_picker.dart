import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../shared/controller/date_picker_controller.dart';
import '../values/colors.dart';

class DatePicker extends StatelessWidget {
  const DatePicker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DateController dateController = Get.put(DateController());

    return GestureDetector(
      onTap: () {
        dateController.chooseDate();
      },
      child: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            const Text('Date', style: TextStyle(color: buttonGreen)),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  DateFormat("dd-MM-yyyy")
                      .format(dateController.selectedDate.value)
                      .toString(),
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                const Icon(Icons.calendar_today_outlined, color: buttonGreen)
              ],
            ),
            Divider(color: buttonGreen),
          ],
        ),
      ),
    );
  }
}
