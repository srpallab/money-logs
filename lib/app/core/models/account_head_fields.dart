class AccountHeadFields {
  static final String accountHeadTableName = 'account_head';
  static final List<String> values = [id, name, type, createdBy, createdAt];
  static const String id = 'id';
  static const String name = 'name';
  static const String type = 'type';
  static const String createdBy = 'created_by';
  static const String createdAt = 'created_at';
}
