import 'package:get/get.dart';

class AccountHeadFirstList {
  String? name;
  RxBool add;

  AccountHeadFirstList({this.name, required this.add});
}
