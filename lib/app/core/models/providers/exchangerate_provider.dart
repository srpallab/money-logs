import 'dart:developer';

import 'package:get/get.dart';

import '../exchangerate_model.dart';

class ExchangeRateProvider extends GetConnect {
  Future<ExchangeRate> getExchangeRate() async {
    final response = await get(
        'https://v6.exchangerate-api.com/v6/f2e87b40162116e2a8c156fe/latest/BDT');
    log(response.status.code.toString());
    //log(response.body.toString());
    return ExchangeRate.fromJson(response.body);
  }
}
