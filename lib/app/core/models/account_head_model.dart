import 'account_head_fields.dart';

class AccountHead {
  int? id;
  late String name;
  late int type;
  late int createdBy;
  late String createAt;

  AccountHead({
    this.id,
    required this.name,
    required this.type,
    required this.createdBy,
    required this.createAt,
  });

  AccountHead.fromJson(Map<String, dynamic> json) {
    id = json[AccountHeadFields.id];
    name = json[AccountHeadFields.name];
    type = json[AccountHeadFields.type];
    createdBy = json[AccountHeadFields.createdBy];
    createAt = json[AccountHeadFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // data[AccountHeadFields.id] = id;
    data[AccountHeadFields.name] = name;
    data[AccountHeadFields.type] = type;
    data[AccountHeadFields.createdBy] = createdBy;
    data[AccountHeadFields.createdAt] = createAt;
    return data;
  }
}
