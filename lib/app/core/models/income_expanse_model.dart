import 'income_expense_fields.dart';

class IncomeExpanse {
  int? id;
  late String amount;
  late String details;
  int? type;
  String? transactionDate;
  String? transactionTime;
  String? headName;
  late int createdBy;
  late String createdAt;

  IncomeExpanse({
    this.id,
    required this.amount,
    required this.details,
    this.type,
    this.transactionDate,
    this.transactionTime,
    this.headName,
    required this.createdBy,
    required this.createdAt,
  });

  IncomeExpanse.fromJson(Map<String, dynamic> json) {
    id = json[IncomeExpenseFields.id];
    amount = json[IncomeExpenseFields.amount];
    details = json[IncomeExpenseFields.details];
    type = json[IncomeExpenseFields.type];
    transactionDate = json[IncomeExpenseFields.transactionDate];
    transactionTime = json[IncomeExpenseFields.transactionTime];
    headName = json[IncomeExpenseFields.headName];
    createdBy = json[IncomeExpenseFields.createdBy];
    createdAt = json[IncomeExpenseFields.createdAt];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data[IncomeExpenseFields.id] = id;
    data[IncomeExpenseFields.amount] = amount;
    data[IncomeExpenseFields.details] = details;
    data[IncomeExpenseFields.type] = type;
    data[IncomeExpenseFields.transactionDate] = transactionDate;
    data[IncomeExpenseFields.transactionTime] = transactionTime;
    data[IncomeExpenseFields.headName] = headName;
    data[IncomeExpenseFields.createdBy] = createdBy;
    data[IncomeExpenseFields.createdAt] = createdAt;
    return data;
  }
}
